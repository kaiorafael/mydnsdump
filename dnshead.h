#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>


#ifndef DNSHEAD_H_				//DNSHEAD.H
#define DNSHEAD_H_

#define DNS_MSG_HDR_SZ 12			//max msg header size
#define MAX_QNAME_SZ 512			//max qname size
#define RFC1035_MAXLABELSZ 63		//max label size

long int total_descard = 0 ;
/*******************************************
 *  DNS Header Struct
 *******************************************/
struct Dnshdr_t {
    unsigned short id;
    unsigned int qr:1;
    unsigned int opcode:4;
    unsigned int aa:1;
    unsigned int tc:1;
    unsigned int rd:1;
    unsigned int ra:1;
    unsigned int rcode:4;
    unsigned short qdcount;
    unsigned short ancount;
    unsigned short nscount;
    unsigned short arcount;
};
typedef struct Dnshdr_t DNShdr;  /* Define Dns Header */

#endif /* DNSHEAD_H_ */
/**
 * This code were write by Duane Wessels
 * from http://dns.measurement-factory.com/
 * This code gets the QNAME and QTYPE from a DNS
 * payload
 */

int rfc1035NameUnpack(const char *buf, size_t sz, off_t * off, char *name, size_t ns)
{

	off_t no = 0;
    unsigned char c;
    size_t len;
    static int loop_detect = 0;

    if (loop_detect > 2){
    	return 4;		/* compression loop */
    }

    if (ns <= 0){
    	return 4;		/* probably compression loop */
    }

    do {

    	if ((*off) >= sz){
    		//printf("Error Offset if bigger than DNS len \n");

    		break;
    	}

    	/* dns header and dns data */
    	c = *(buf + (*off));

    	if (c > 191) {

    		/* blasted compression */
    		int rc;
    		unsigned short s;
    		off_t ptr;

    		memcpy(&s, buf + (*off), sizeof(s));
	    	s = ntohs(s);
	    	(*off) += sizeof(s);

	    	/* Sanity check */
	    	if ((*off) >= sz) {
	    		total_descard++;
	    		return 1;	/* message too short */

	    	}

	    	ptr = s & 0x3FFF;

	    	/* Make sure the pointer is inside this message */
	    	if (ptr >= sz){
	    		total_descard++;
	    		return 2;	/* bad compression ptr */
	    	}

	    /* Test if 'ptr'is less than pkt header */
	    if (ptr < DNS_MSG_HDR_SZ){
	    	total_descard++;
	    	return 2;	/* bad compression ptr */
	    }

	    loop_detect++;
	    rc = rfc1035NameUnpack(buf, sz, &ptr, name + no, ns - no);
	    loop_detect--;

	    return rc;
    	} else if (c > RFC1035_MAXLABELSZ) {
	    /*
	     * "(The 10 and 01 combinations are reserved for future use.)"
	     */
    		total_descard++;
    		return 3;		/* reserved label/compression flags */
    		break;
    	} else {

    		(*off)++;
    		len = (size_t) c;

    		if (len == 0){
    			break;
    		}

    		if (len > (ns - 1)){
    			len = ns - 1;
    		}

    		if ((*off) + len > sz){
    			total_descard++;
    			return 4;	/* message is too short */
    		}

    		if (no + len + 1 > ns){
    			total_descard++;
    			return 5;	/* qname would overflow name buffer */
    		}

    		/* missing some explanation here */
    		memcpy(name + no, buf + (*off), len);
    		(*off) += len;
    		no += len;
    		*(name + (no++)) = '.';
    	}
    } while (c > 0); // do while statement ends..


    if (no > 0){
    	*(name + no - 1) = '\0';
    }

    /* make sure we didn't allow someone to overflow the name buffer */
    assert(no <= ns);

    return 0;
}
