#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libtrace.h> 
#include <assert.h>
#include "dnsdump.h"

#if defined (__linux__) || defined (__GLIBC__) || defined (__GNU__)
#define uh_dport dest
#define uh_sport source
#endif

int main(int argc, char *argv[])
{

        libtrace_t *trace = NULL;
        libtrace_packet_t *packet = NULL;

	int i=1, x=0;								/* Count packet and Payload error control */


        if (argc < 2) {
                fprintf(stderr, "Usage: %s inputURI\n", argv[0]);
                return 1;
        }

    	packet = trace_create_packet();						/* Initializing*/

        if (packet == NULL) {
                perror("Creating libtrace packet");
                libtrace_cleanup(trace, packet);
                return 1;
        }

        trace = trace_create(argv[1]);						/* Initializing*/

        if (trace_is_err(trace)) {
                trace_perror(trace,"Opening trace file");
                libtrace_cleanup(trace, packet);
                return 1;
        }


        if (trace_start(trace) == -1) {
                trace_perror(trace,"Starting trace");
                libtrace_cleanup(trace, packet);
                return 1;
        }


        while (trace_read_packet(trace,packet)>0) {				/* Main function*/
                dnsdump(packet, &i, &x);
		i++;
        }
	print_all();
        if (trace_is_err(trace)) {
                trace_perror(trace,"Reading packets");
                libtrace_cleanup(trace, packet);
                return 1;
        }

        libtrace_cleanup(trace, packet);
        return 0;
}

//#endif    /*IP*/
//#endif    /*ETHERNET*/
