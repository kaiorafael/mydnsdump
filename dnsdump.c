#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libtrace.h> 
#include <assert.h>
#include "dnsdump.h"
#include "dnshead.h"
#include "hash.h"

void dnsdump (libtrace_packet_t *packet, int *i, int *x)
{
	struct sockaddr_storage addr;
	struct sockaddr *addr_ptr;
	unsigned short us = 0, qtype = 0;
    	printf("i = %d\n",*i);

	int etherOffsetLen = trace_get_capture_length(packet);			/* Get capture lenght */

										/* ethernet pkt layer */
	const struct libtrace_ether *ethernet = NULL;    			/* ethernet header */
	int size_ethernet;							/* ethernet size */

										/* IP pkts layer */
	const libtrace_ip_t *ip = NULL;                    			/* IP header */
	int ipOffsetLen = 0 ;                        				/* IP Offset lenght*/
	int size_ip;                                				/* IP size */
	
										/* UDP pkt layer */
	libtrace_udp_t *udpPkt = NULL;                				/* UDP pkt */    
	int UdpOffsetLen;                            				/* UDP Offset lenght*/

										/* DNS pkt layer */
	int DnsOffsetLen;                            				/* DNS pkt lenght*/
	off_t offset;
										/* for Loop layer */
    	unsigned int family;

									        /* EtherNet Packet layer*/
	size_ethernet = 14;							/* Ethernet headers are always exactly 14 bytes */

    	ethernet = (struct libtrace_ether *) (packet);				/* define ethernet header */

    	etherOffsetLen = etherOffsetLen - size_ethernet;			/* etherOffsetLen has size of IP + UDP + DNS */
    	
    	libtrace_udp_t *udp;    						/* Generic UDP header structure */
    	char *dnsPkt;          							/* DNS Pkt Pointer */
    	DNShdr query_header;    						/* DNS STRUCT, dnshead.h*/

    	udp = trace_get_udp(packet);						/* Get UDP Header */
    	
    	if (udp == NULL)							/* Error to Get UDP */
        	return;
   										/* DNS Verification*/
	if ((ntohs(udp->source) != 53) && ( ntohs(udp->dest) != 53)) 		/* We want only UDP */
        	return ;
    
    	dnsPkt = (char *) (udp + 1);						/* defines the DNS pkt */

    										/* DNS ID : 16 bits, 2 bytes */
    	memcpy(&us, dnsPkt + 00, 2);
    	query_header.id = ntohs(us);
    	printf("DNS ID %u \n", query_header.id);
    	    									/* Gets DNS Header Flags */
  	memcpy(&us, dnsPkt + 2, 2);
    	us = ntohs(us);

    	query_header.qr = (us >> 15) & 0x01;        
    	query_header.opcode = (us >> 11) & 0x0F;    				/* Get query header opcode */
    	query_header.aa = (us >> 10) & 0x01;        				/* Get authoritive answer */
    	query_header.tc = (us >> 9) & 0x01;            				/* Get truncate answer */
    	query_header.rd = (us >> 8) & 0x01;            				/* Get recursion desired */
    	query_header.ra = (us >> 7) & 0x01;            				/* Get recursion available */
	//query_header.ad = (us >> 5) & 0x01;     	       			/* Get Authentic Data  */ 
	//query_header.cd = (us >> 4) & 0x01;	            			/* Get Checking Disabled */
    	query_header.rcode = us & 0x0F;                				/* Get response code */
	
										/* Get QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT */
    										/* Number of question*/
    	if(!memcpy(&us, dnsPkt + 4, 2)){
	        printf("Could not allocate to line 408\n");
	}
	else{
		query_header.qdcount = ntohs(us);
     	}

										/* Answer count */
	if(!memcpy(&us, dnsPkt + 6, 2)){
        	printf("Could not allocate to line 408\n");
    	}
	else{
           query_header.ancount = ntohs(us);
    	}

    										/* authority */
    	if(!memcpy(&us, dnsPkt + 8, 2)){
		printf("Could not allocate to line 424\n");

    	}
	else{
        query_header.nscount = ntohs(us);
	}

    										/* additional count*/
    	if(!memcpy(&us, dnsPkt + 10, 2)){
        	printf("Could not allocate to line 431 \n");
    	}
	else{
        	query_header.arcount = ntohs(us);
	}
    	
	offset = sizeof(query_header);							/* Get Query Header size */
	char qname[MAX_QNAME_SZ];         						/* Waiting QNAME */
    	*x = rfc1035NameUnpack(dnsPkt, DnsOffsetLen, &offset, qname, MAX_QNAME_SZ);	/* *x has to be 0, else Error. Get QNAME*/

   	if (0 != *x){
        	printf("Erro to open the DNS payload %d\n",*x);
        	return;
    	}
   
										/* Get QTYPE*/
	if(!memcpy(&us, dnsPkt + offset, 2)){					/* Error Verification*/
        	printf("Could not get the QTYPE\n");
        }	
	else{
	
        	qtype = ntohs(us);						/* Define QTYPE*/ 
		printf("%s\n",qname);
		//printing(qtype, qname);		
		put_hash(qtype);						//Usa o hash.c	
				
	}

	addr_ptr = trace_get_source_address(packet, (struct sockaddr *)&addr);	/* Print IP*/
	printf("\n");
}

			            
void libtrace_cleanup (libtrace_t *trace, libtrace_packet_t *packet) {

        if (trace)
                trace_destroy(trace);

        if (packet)
                trace_destroy_packet(packet);

}
